/*
 * queue.c
 *
 *  Created on: 11.11.2018
 *      Author: Piotr
 */

#include "queue.h"
#include <stdlib.h>

Queue* queue_create(uint8_t size)
{
	Queue* queue = (Queue*) malloc(sizeof(Queue));
	queue->queue_ = (int8_t*) malloc(size);
	queue->capacity_ = size;
	queue->size_ = 0;
	queue->indexFirstElement_ = 0;
	queue->indexLastElement_ = 0;
	return queue;
}

void queue_destroy(Queue* queue)
{
	free(queue->queue_);
	free(queue);
}

int8_t queue_front(Queue* queue)
{
	return queue->queue_[queue->indexFirstElement_];
}

int8_t queue_pop(Queue* queue)
{
	queue->size_--;
	int8_t data = queue->queue_[queue->indexFirstElement_++];
	queue->indexFirstElement_ %= queue->capacity_;
	return data;
}

uint8_t queue_push(Queue* queue, int8_t data)
{
	if (queue_isFull(queue))
	{
		return 1;
	}
	queue->size_++;
	queue->queue_[queue->indexLastElement_] = data;
	queue->indexLastElement_ = (queue->indexLastElement_ + 1)
			% queue->capacity_;
	return 0;
}

uint8_t queue_isFull(Queue* queue)
{
	return queue->size_ == queue->capacity_;
}

uint8_t queue_isEmpty(Queue* queue)
{
	return queue->size_ == 0;
}

uint8_t queue_checkSeq(Queue* queue)
{
	return queue->size_ >= 3 && queue->queue_[queue->indexFirstElement_] >= -100
			&& queue->queue_[queue->indexFirstElement_] <= 100
			&& queue->queue_[(queue->indexFirstElement_ + 1) % queue->capacity_]
					>= -100
			&& queue->queue_[(queue->indexFirstElement_ + 1) % queue->capacity_]
					<= 100
			&& queue->queue_[(queue->indexFirstElement_ + 2) % queue->capacity_]
					== -101
			&& queue->queue_[(queue->indexFirstElement_ + 2) % queue->capacity_]
					== 101;
}
