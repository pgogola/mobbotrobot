/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2018 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include "queue.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
Queue* queue;
extern DMA_HandleTypeDef hdma_usart1_rx;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

volatile uint8_t rec;
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	rec = 1;
	printf("rec 1 \r\n");
}

int _write(int file, char *data, int len)
{
//	HAL_UART_Transmit_DMA(&huart2, (uint8_t*) data, (uint16_t)len);
	HAL_UART_Transmit(&huart2, (uint8_t*) data, (uint16_t) len, 100);
	return len;
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
	MX_TIM3_Init();
	MX_ADC1_Init();
	/* USER CODE BEGIN 2 */
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */

	printf("Dzialam\r\n");

//	sendCommandToBluetooth("AT+ORGL\r\n", 10);
//	HAL_Delay(10);
//	receiveFromBluetooth(blhReceiveDataBuffor, 4);
//	HAL_Delay(1000);
//	sendCommandToBluetooth("AT+ADDR?\r\n", 11);
//	HAL_Delay(10);
//	receiveFromBluetooth(blhReceiveDataBuffor, 26);
//	HAL_Delay(1000);
//	sendCommandToBluetooth("AT+NAME=RM2\r\n", 14);
//	HAL_Delay(10);
//	receiveFromBluetooth(blhReceiveDataBuffor, 4);
	uint8_t buffor[20];

	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);

	__HAL_TIM_SetCompare(&htim3, TIM_CHANNEL_1, 0);
	__HAL_TIM_SetCompare(&htim3, TIM_CHANNEL_2, 0);

	HAL_GPIO_WritePin(STBY_GPIO_Port, STBY_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(PAIN1_GPIO_Port, PAIN1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(PAIN2_GPIO_Port, PAIN2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(PBIN1_GPIO_Port, PBIN1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(PBIN2_GPIO_Port, PBIN2_Pin, GPIO_PIN_RESET);

//	HAL_Delay(1000);
	volatile int8_t right;
	volatile int8_t left;

	HAL_Delay(1000);
	__HAL_TIM_SetCompare(&htim3, TIM_CHANNEL_1, 0);
	__HAL_TIM_SetCompare(&htim3, TIM_CHANNEL_2, 0);
	rec = 0;
//	HAL_UART_Receive_IT(&huart1, buffor, 3);
	volatile uint8_t flag = 0;
	while (1)
	{
		HAL_ADC_Start(&hadc1);
		HAL_Delay(100);
		printf("B %lu\r\n", HAL_ADC_GetValue(&hadc1));
		while (HAL_OK != HAL_UART_Receive(&huart1, buffor, 1, 100)
				|| buffor[0] == 101)
		{
			printf("101\r\n");
		};
		right = buffor[0];
		if (right == 101)
		{
			continue;
		}
		while (HAL_OK != HAL_UART_Receive(&huart1, buffor, 1, 10))
			;
		left = buffor[0];
		if (left == 101)
		{
			continue;
		}
		printf("rec %lu %lu\r\n", right, left);
		if (right > 0)
		{
			HAL_GPIO_WritePin(PAIN1_GPIO_Port, PAIN1_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(PAIN2_GPIO_Port, PAIN2_Pin, GPIO_PIN_SET);

		} else
		{
			HAL_GPIO_WritePin(PAIN1_GPIO_Port, PAIN1_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(PAIN2_GPIO_Port, PAIN2_Pin, GPIO_PIN_RESET);
			right *= -1;
		}

		if (left > 0)
		{
			HAL_GPIO_WritePin(PBIN1_GPIO_Port, PBIN1_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(PBIN2_GPIO_Port, PBIN2_Pin, GPIO_PIN_SET);

		} else
		{
			HAL_GPIO_WritePin(PBIN1_GPIO_Port, PBIN1_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(PBIN2_GPIO_Port, PBIN2_Pin, GPIO_PIN_RESET);
			left *= -1;
		}
		printf("in %d  %d\r\n", right, left);
		__HAL_TIM_SetCompare(&htim3, TIM_CHANNEL_1, right);
		__HAL_TIM_SetCompare(&htim3, TIM_CHANNEL_2, left);
		rec = 0;
	}

	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */

	/* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE()
	;

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 50;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
