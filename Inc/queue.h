/*
 * queue.h
 *
 *  Created on: 11.11.2018
 *      Author: Piotr
 */

#include "stdint.h"

#ifndef QUEUE_C_
#define QUEUE_C_

typedef struct {
	int8_t* queue_;
	uint8_t size_;
	uint8_t capacity_;
	uint8_t indexFirstElement_;
	uint8_t indexLastElement_;
} Queue;

Queue* queue_create(uint8_t size);
void queue_destroy(Queue* queue);
int8_t queue_front(Queue* queue);
int8_t queue_pop(Queue* queue);
uint8_t queue_push(Queue* queue, int8_t data);
uint8_t queue_isFull(Queue* queue);
uint8_t queue_isEmpty(Queue* queue);
uint8_t queue_checkSeq(Queue* queue);

#endif /* QUEUE_C_ */
